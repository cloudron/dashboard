# Cloudron Dashboard

This is the front end code of Cloudron. The backend code is [here](https://git.cloudron.io/cloudron/box).

## Developing

* `npm install`
* `gulp develop --api-origin=https://my.example.com`

## License

Please note that the Cloudron code is under a source-available license. This is not the same as an
open source license but ensures the code is available for inspection (and hacking!).

## Contributions

Just to give a heads-up, we are a bit restrictive in merging changes. We are a small team and
would like to keep our maintenance burden low. It might be best to first discuss features in the [forum](https://forum.cloudron.io),
which also helps to determine how many other people will use it to justify maintenance for a feature.

