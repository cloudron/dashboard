'use strict';

/* global angular */
/* global $ */

angular.module('Application').controller('NetworkController', ['$scope', '$location', 'Client', function ($scope, $location, Client) {
    Client.onReady(function () { if (!Client.getUserInfo().isAtLeastAdmin) $location.path('/'); });

    $scope.user = Client.getUserInfo();
    $scope.config = Client.getConfig();

    // keep in sync with sysinfo.js
    $scope.sysinfoProvider = [
        { name: 'Public IP', value: 'generic' },
        { name: 'Static IP Address', value: 'fixed' },
        { name: 'Network Interface', value: 'network-interface' }
    ];

    $scope.ipv6ConfigureProvider = [
        { name: 'Disabled', value: 'noop' },
        { name: 'Public IP', value: 'generic' },
        { name: 'Static IP Address', value: 'fixed' },
        { name: 'Network Interface', value: 'network-interface' }
    ];

    $scope.prettyIpProviderName = function (provider) {
        switch (provider) {
        case 'noop': return 'Disabled';
        case 'generic': return 'Public IP';
        case 'fixed': return 'Static IP Address';
        case 'network-interface': return 'Network Interface';
        default: return 'Unknown';
        }
    };

    $scope.dyndnsConfigure = {
        busy: false,
        error: '',
        isEnabled: false,

        refresh: function () {
            Client.getDynamicDnsConfig(function (error, enabled) {
                if (error) return console.error(error);

                $scope.dyndnsConfigure.isEnabled = enabled;
            });
        },

        setEnabled: function (enabled) {
            $scope.dyndnsConfigure.busy = true;
            $scope.dyndnsConfigure.error = '';

            Client.setDynamicDnsConfig(enabled, function (error) {
                $scope.dyndnsConfigure.busy = false;

                if (error) $scope.dyndnsConfigure.error = error.message;
                else $scope.dyndnsConfigure.isEnabled = enabled;
            });
        }
    };

    $scope.ipv6Configure = {
        busy: false,
        error: {},
        displayError: '',

        serverIPv6: '',

        provider: '',
        ipv6: '',
        ifname: '',

        // configure dialog
        newProvider: '',
        newIPv6: '',
        newIfname: '',

        refresh: function () {
            Client.getIPv6Config(function (error, result) {
                if (error) {
                    $scope.ipv6Configure.displayError = error.message;
                    return console.error(error);
                }

                $scope.ipv6Configure.provider = result.provider;
                $scope.ipv6Configure.ipv6 = result.ipv6 || '';
                $scope.ipv6Configure.ifname = result.ifname || '';
                if (result.provider === 'noop') return;

                Client.getServerIpv6(function (error, result) {
                    if (error) {
                        $scope.ipv6Configure.displayError = error.message;
                        return console.error(error);
                    }

                    $scope.ipv6Configure.serverIPv6 = result.ipv6;
                });
            });
        },

        show: function () {
            $scope.ipv6Configure.error = {};
            $scope.ipv6Configure.newProvider = $scope.ipv6Configure.provider;
            $scope.ipv6Configure.newIPv6 = $scope.ipv6Configure.ipv6;
            $scope.ipv6Configure.newIfname = $scope.ipv6Configure.ifname;

            $('#ipv6ConfigureModal').modal('show');
        },

        submit: function () {
            $scope.ipv6Configure.error = {};
            $scope.ipv6Configure.busy = true;

            var config = {
                provider: $scope.ipv6Configure.newProvider
            };

            if (config.provider === 'fixed') {
                config.ipv6 = $scope.ipv6Configure.newIPv6;
            } else if (config.provider === 'network-interface') {
                config.ifname = $scope.ipv6Configure.newIfname;
            }

            Client.setIPv6Config(config, function (error) {
                $scope.ipv6Configure.busy = false;
                if (error && error.message.indexOf('ipv') !== -1) {
                    $scope.ipv6Configure.error.ipv6 = error.message;
                    $scope.ipv6ConfigureForm.$setPristine();
                    $scope.ipv6ConfigureForm.$setUntouched();
                    return;
                } else if (error && (error.message.indexOf('interface') !== -1 || error.message.indexOf('IPv6') !== -1)) {
                    $scope.ipv6Configure.error.ifname = error.message;
                    $scope.ipv6ConfigureForm.$setPristine();
                    $scope.ipv6ConfigureForm.$setUntouched();
                    return;
                } else if (error) {
                    $scope.ipv6Configure.error.generic = error.message;
                    $scope.ipv6ConfigureForm.$setPristine();
                    $scope.ipv6ConfigureForm.$setUntouched();
                    console.error(error);
                    return;
                }

                $scope.ipv6Configure.refresh();

                $('#ipv6ConfigureModal').modal('hide');
            });
        }
    };

    $scope.blocklist  = {
        busy: false,
        error: {},
        blocklist: '',
        currentBlocklist: '',
        currentBlocklistLength: 0,

        refresh: function () {
            Client.getBlocklist(function (error, result) {
                if (error) return console.error(error);

                $scope.blocklist.currentBlocklist = result;
                $scope.blocklist.currentBlocklistLength = result.split('\n').filter(function (l) { return l.length !== 0 && l[0] !== '#'; }).length;
            });
        },

        show: function () {
            $scope.blocklist.error = {};
            $scope.blocklist.blocklist = $scope.blocklist.currentBlocklist;

            $('#blocklistModal').modal('show');
        },

        submit: function () {
            $scope.blocklist.error = {};
            $scope.blocklist.busy = true;

            Client.setBlocklist($scope.blocklist.blocklist, function (error) {
                $scope.blocklist.busy = false;
                if (error) {
                    $scope.blocklist.error.blocklist = error.message;
                    $scope.blocklist.error.ip = error.message;
                    $scope.blocklistChangeForm.$setPristine();
                    $scope.blocklistChangeForm.$setUntouched();
                    return;
                }

                $scope.blocklist.refresh();

                $('#blocklistModal').modal('hide');
            });
        }
    };

    $scope.sysinfo = {
        busy: false,
        error: {},

        serverIPv4: '',

        provider: '',
        ipv4: '',
        ifname: '',

        // configure dialog
        newProvider: '',
        newIPv4: '',
        newIfname: '',

        refresh: function () {
            Client.getSysinfoConfig(function (error, result) {
                if (error) return console.error(error);

                $scope.sysinfo.provider = result.provider;
                $scope.sysinfo.ipv4 = result.ipv4 || '';
                $scope.sysinfo.ifname = result.ifname || '';

                Client.getServerIpv4(function (error, result) {
                    if (error) return console.error(error);

                    $scope.sysinfo.serverIPv4 = result.ipv4;
                });
            });
        },

        show: function () {
            $scope.sysinfo.error = {};
            $scope.sysinfo.newProvider = $scope.sysinfo.provider;
            $scope.sysinfo.newIPv4 = $scope.sysinfo.ipv4;
            $scope.sysinfo.newIfname = $scope.sysinfo.ifname;

            $('#sysinfoModal').modal('show');
        },

        submit: function () {
            $scope.sysinfo.error = {};
            $scope.sysinfo.busy = true;

            var config = {
                provider: $scope.sysinfo.newProvider
            };

            if (config.provider === 'fixed') {
                config.ipv4 = $scope.sysinfo.newIPv4;
            } else if (config.provider === 'network-interface') {
                config.ifname = $scope.sysinfo.newIfname;
            }

            Client.setSysinfoConfig(config, function (error) {
                $scope.sysinfo.busy = false;
                if (error && error.message.indexOf('ipv') !== -1) {
                    $scope.sysinfo.error.ipv4 = error.message;
                    $scope.sysinfoForm.$setPristine();
                    $scope.sysinfoForm.$setUntouched();
                    return;
                } else if (error && (error.message.indexOf('interface') !== -1 || error.message.indexOf('IPv4') !== -1)) {
                    $scope.sysinfo.error.ifname = error.message;
                    $scope.sysinfoForm.$setPristine();
                    $scope.sysinfoForm.$setUntouched();
                    return;
                } else if (error) {
                    $scope.sysinfo.error.generic = error.message;
                    console.error(error);
                    return;
                }

                $scope.sysinfo.refresh();

                $('#sysinfoModal').modal('hide');
            });
        }
    };

    Client.onReady(function () {
        $scope.sysinfo.refresh();

        $scope.dyndnsConfigure.refresh();
        $scope.ipv6Configure.refresh();
        if ($scope.user.isAtLeastOwner) $scope.blocklist.refresh();
    });

    $('.modal-backdrop').remove();
}]);
