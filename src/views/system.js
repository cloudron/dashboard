'use strict';

/* global angular */
/* global $ */
/* global Chart */

angular.module('Application').controller('SystemController', ['$scope', '$location', '$timeout', 'Client', function ($scope, $location, $timeout, Client) {
    Client.onReady(function () { if (!Client.getUserInfo().isAtLeastAdmin) $location.path('/'); });

    $scope.config = Client.getConfig();
    $scope.memory = null;
    $scope.volumesById = {};

    // https://stackoverflow.com/questions/1484506/random-color-generator
    function rainbow(numOfSteps, step) {
        // This function generates vibrant, "evenly spaced" colours (i.e. no clustering). This is ideal for creating easily distinguishable vibrant markers in Google Maps and other apps.
        // Adam Cole, 2011-Sept-14
        // HSV to RBG adapted from: http://mjijackson.com/2008/02/rgb-to-hsl-and-rgb-to-hsv-color-model-conversion-algorithms-in-javascript
        var r, g, b;
        var h = step / numOfSteps;
        var i = ~~(h * 6);
        var f = h * 6 - i;
        var q = 1 - f;
        switch(i % 6){
        case 0: r = 1; g = f; b = 0; break;
        case 1: r = q; g = 1; b = 0; break;
        case 2: r = 0; g = 1; b = f; break;
        case 3: r = 0; g = q; b = 1; break;
        case 4: r = f; g = 0; b = 1; break;
        case 5: r = 1; g = 0; b = q; break;
        }
        var c = '#' + ('00' + (~ ~(r * 255)).toString(16)).slice(-2) + ('00' + (~ ~(g * 255)).toString(16)).slice(-2) + ('00' + (~ ~(b * 255)).toString(16)).slice(-2);
        return (c);
    }

    // https://stackoverflow.com/questions/6274339/how-can-i-shuffle-an-array
    function shuffle(a) {
        for (let i = a.length - 1; i > 0; i--) {
            const j = Math.floor(Math.random() * (i + 1));
            [a[i], a[j]] = [a[j], a[i]];
        }
        return a;
    }

    var colorIndex = 0;
    var colors = [];
    function resetColors(n) {
        colorIndex = 0;
        colors = [];
        for (var i = 0; i < n; i++) colors.push(rainbow(n, i));
        shuffle(colors);
    }

    function getNextColor() {
        return colors[colorIndex++];
    }

    $scope.disks = {
        busy: true,
        busyRefresh: false,
        ts: 0,
        taskId: '',
        disks: [],

        show: function () {
            Client.diskUsage(function (error, result) {
                if (error) return console.error('Failed to refresh disk usage.', error);

                if (!result.usage) {
                    $scope.disks.busy = false;
                    return;
                }

                $scope.disks.ts = result.usage.ts;

                // [ { filesystem, type, size, used, available, capacity, mountpoint }]
                $scope.disks.disks = Object.keys(result.usage.disks).map(function (k) { return result.usage.disks[k]; });

                $scope.disks.disks.forEach(function (disk) {
                    var usageOther = disk.used;

                    resetColors(disk.contents.length);

                    // if this disk is a volume amend it and remove it from contents
                    disk.contents.forEach(function (content) { if (content.path === disk.mountpoint) disk.volume = $scope.volumesById[content.id]; });
                    disk.contents = disk.contents.filter(function (content) { return content.path !== disk.mountpoint; });

                    disk.contents.forEach(function (content) {
                        content.color = getNextColor();

                        if (content.type === 'app') {
                            content.app = Client.getInstalledAppsByAppId()[content.id];
                            if (!content.app) content.uninstalled = true;
                        }
                        if (content.type === 'volume') content.volume = $scope.volumesById[content.id];

                        usageOther -= content.usage;
                    });

                    disk.contents.sort(function (x, y) { return y.usage - x.usage; }); // sort by usage

                    if ($scope.disks.disks[0] === disk) { // the root mount point is the first disk. keep this 'contains' in the end
                        disk.contents.push({
                            type: 'standard',
                            label: 'Everything else (Ubuntu, etc)',
                            id: 'other',
                            color: '#555555',
                            usage: usageOther
                        });
                    } else {
                        disk.contents.push({
                            type: 'standard',
                            label: 'Used',
                            id: 'other',
                            color: '#555555',
                            usage: usageOther
                        });
                    }
                });

                $scope.disks.busy = false;
            });
        },

        checkStatus: function () {
            Client.getLatestTaskByType(TASK_TYPES.TASK_UPDATE_DISK_USAGE, function (error, task) {
                if (error) return console.error(error);

                if (!task) return;

                $scope.disks.taskId = task.id;
                $scope.disks.busyRefresh = true;
                $scope.disks.updateStatus();
            });
        },

        updateStatus: function () {
            Client.getTask($scope.disks.taskId, function (error, data) {
                if (error) return $timeout($scope.disks.updateStatus, 3000);

                if (!data.active) {
                    $scope.disks.busyRefresh = false;
                    $scope.disks.taskId = '';
                    $scope.disks.show();
                    return;
                }

                $timeout($scope.disks.updateStatus, 3000);
            });
        },

        refresh: function () {
            $scope.disks.busyRefresh = true;

            Client.refreshDiskUsage(function (error, taskId) {
                if (error) {
                    $scope.disks.busyRefresh = false;
                    return console.error('Failed to refresh disk usage.', error);
                }

                $scope.disks.taskId = taskId;
                $timeout($scope.disks.updateStatus, 3000);
            });
        }
    };

    $scope.graphs = {
        busy: false,
        period: 6,
        memoryChart: null,
        diskChart: null,

        setPeriod: function (hours) {
            $scope.graphs.period = hours;
            $scope.graphs.refresh();
        },

        refresh: function () {
            $scope.graphs.busy = true;

            Client.getSystemGraphs($scope.graphs.period * 60, function (error, result) {
                if (error) return console.error('Failed to fetch system graphs:', error);

                var cpuCount = result.cpuCount;

                // in minutes
                var timePeriod = $scope.graphs.period * 60;

                // keep in sync with graphs.js
                var timeBucketSizeMinutes = timePeriod > (24 * 60) ? (6*60) : 5;
                var steps = Math.floor(timePeriod/timeBucketSizeMinutes);

                var labels = new Array(steps).fill(0);
                labels = labels.map(function (v, index) {
                    var dateTime = new Date(Date.now() - ((timePeriod - (index * timeBucketSizeMinutes)) * 60 * 1000));

                    if ($scope.graphs.period > 24) {
                        return dateTime.toLocaleDateString();
                    } else {
                        return dateTime.toLocaleTimeString();
                    }
                });

                function fillGraph(canvasId, contents, chartPropertyName, divisor, max, format, formatDivisor) {
                    if (!contents || !contents[0]) return; // no data available yet

                    var datasets = [];

                    resetColors(contents.length);
                    contents.forEach(function (content, index) {

                        // fill holes with previous value
                        var cur = 0;
                        content.data.forEach(function (d) {
                            if (d[0] === null) d[0] = cur;
                            else cur = d[0];
                        });

                        var datapoints = Array(steps).map(function () { return '0'; });

                        // walk backwards and fill up the datapoints
                        content.data.reverse().forEach(function (d, index) {
                            datapoints[datapoints.length-1-index] = (d[0] / divisor).toFixed(2);
                        });

                        var color = index === 0 ? '#2196F3' : getNextColor();
                        datasets.push({
                            label: content.label,
                            backgroundColor: color + '4F',
                            borderColor: color, // FIXME give real distinct colors
                            borderWidth: 1,
                            radius: 0,
                            data: datapoints,
                            cubicInterpolationMode: 'monotone',
                            tension: 0.4
                        });
                    });

                    var graphData = {
                        labels: labels,
                        datasets: datasets
                    };

                    var options = {
                        responsive: true,
                        maintainAspectRatio: true,
                        aspectRatio: 2.5,
                        animation: false,
                        plugins: {
                            legend: {
                                display: false
                            }
                        },
                        interaction: {
                            intersect: false,
                            mode: 'index',
                        },
                        scales: {
                            x: {
                                ticks: { autoSkipPadding: 50, maxRotation: 0 }
                            },
                            y: {
                                ticks: { maxTicksLimit: 6 },
                                min: 0,
                                beginAtZero: true
                            }
                        }
                    };

                    if (format) options.scales.y.ticks.callback = function (value) { return (formatDivisor ? (value/formatDivisor).toFixed(0) : value) + ' ' + format; };
                    if (max) options.scales.y.max = max;

                    var ctx = $(canvasId).get(0).getContext('2d');

                    if ($scope.graphs[chartPropertyName]) $scope.graphs[chartPropertyName].destroy();
                    $scope.graphs[chartPropertyName] = new Chart(ctx, { type: 'line', data: graphData, options: options });
                }

                var cpuThreshold = 20;
                var appsWithHighCPU = Object.keys(result.apps).map(function (appId) {
                    result.apps[appId].id = appId;

                    var app = Client.getInstalledAppsByAppId()[appId];
                    if (!app) result.apps[appId].label = appId;
                    else result.apps[appId].label = app.label || app.fqdn;

                    return result.apps[appId];
                }).filter(function (app) {
                    if (!app.cpu) return false; // not sure why we get empty objects
                    return app.cpu.some(function (d) { return d[0] > cpuThreshold; });
                }).map(function (app) {
                    return { data: app.cpu, label: app.label };
                });

                var memoryThreshold = 1024 * 1024 * 1024;
                var appsWithHighMemory = Object.keys(result.apps).map(function (appId) {
                    result.apps[appId].id = appId;

                    var app = Client.getInstalledAppsByAppId()[appId];
                    if (!app) result.apps[appId].label = appId;
                    else result.apps[appId].label = app.label || app.fqdn;

                    return result.apps[appId];
                }).filter(function (app) {
                    if (!app.memory) return false; // not sure why we get empty objects
                    return app.memory.some(function (d) { return d[0] > memoryThreshold; });
                }).map(function (app) {
                    return { data: app.memory, label: app.label };
                });

                fillGraph('#graphsCPUChart', [{ data: result.cpu, label: 'CPU' }].concat(appsWithHighCPU), 'cpuChart', 1, cpuCount * 100, '%');
                fillGraph('#graphsSystemMemoryChart', [{ data: result.memory, label: 'Memory' }].concat(appsWithHighMemory), 'memoryChart', 1024 * 1024, Number.parseInt($scope.memory.memory / 1024 / 1024), 'GiB', 1024);

                $scope.graphs.busy = false;
            });
        }
    };

    Client.onReady(function () {
        Client.memory(function (error, memory) {
            if (error) console.error(error);

            $scope.memory = memory;

            Client.getVolumes(function (error, volumes) {
                if (error) return console.error(error);

                $scope.volumesById = {};
                volumes.forEach(function (v) { $scope.volumesById[v.id] = v; });

                $scope.graphs.refresh();

                $scope.disks.show();
                $scope.disks.checkStatus();
            });
        });
    });

    Client.onReconnect(function () {
        $scope.reboot.busy = false;
    });
}]);
