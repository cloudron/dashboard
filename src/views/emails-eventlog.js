'use strict';

/* global $ */
/* global angular */

angular.module('Application').controller('EmailsEventlogController', ['$scope', '$location', '$translate', '$timeout', 'Client', function ($scope, $location, $translate, $timeout, Client) {
    Client.onReady(function () { if (!Client.getUserInfo().isAtLeastOwner) $location.path('/'); });

    $scope.ready = false;
    $scope.config = Client.getConfig();
    $scope.user = Client.getUserInfo();

    $scope.pageItemCount = [
        { name: $translate.instant('main.pagination.perPageSelector', { n: 20 }), value: 20 },
        { name: $translate.instant('main.pagination.perPageSelector', { n: 50 }), value: 50 },
        { name: $translate.instant('main.pagination.perPageSelector', { n: 100 }), value: 100 }
    ];

    $scope.activityTypes = [
        { name: 'Bounce', value: 'bounce' },
        { name: 'Deferred', value: 'deferred' },
        { name: 'Delivered', value: 'delivered' },
        { name: 'Denied', value: 'denied' },
        { name: 'Queued', value: 'queued' },
        { name: 'Quota', value: 'quota' },
        { name: 'Received', value: 'received' },
        { name: 'Spam', value: 'spam' },
    ];

    $scope.activity = {
        busy: true,
        eventLogs: [],
        activeEventLog: null,
        currentPage: 1,
        perPage: 20,
        pageItems: $scope.pageItemCount[0],
        selectedTypes: [],
        search: '',

        refresh: function () {
            $scope.activity.busy = true;

            var types = $scope.activity.selectedTypes.map(function (a) { return a.value; }).join(',');

            Client.getMailEventLogs($scope.activity.search, types, $scope.activity.currentPage, $scope.activity.pageItems.value, function (error, result) {
                if (error) return console.error('Failed to fetch mail eventlogs.', error);

                $scope.activity.busy = false;

                $scope.activity.eventLogs = result;
            });
        },

        showNextPage: function () {
            $scope.activity.currentPage++;
            $scope.activity.refresh();
        },

        showPrevPage: function () {
            if ($scope.activity.currentPage > 1) $scope.activity.currentPage--;
            else $scope.activity.currentPage = 1;
            $scope.activity.refresh();
        },

        showEventLogDetails: function (eventLog) {
            if ($scope.activity.activeEventLog === eventLog) $scope.activity.activeEventLog = null;
            else $scope.activity.activeEventLog = eventLog;
        },

        updateFilter: function (fresh) {
            if (fresh) $scope.activity.currentPage = 1;
            $scope.activity.refresh();
        }
    };

    Client.onReady(function () {
        $scope.ready = true;

        $scope.activity.refresh();
    });

    $('.modal-backdrop').remove();
}]);
